![ Build ] (https://bitbucket-badges.atlassian.io/badge/pinturic/react-service.svg)
![ Version ](https://img.shields.io/npm/v/@raas.svg)
![ Downloads ](https://img.shields.io/npm/dm/@raas.svg)
![ Issues ](https://img.shields.io/bitbucket/issues-raw/pinturic/react-service.svg)
![ Code documentation ](./esdoc/badge.svg)

![raas](https://bitbucket.org/pinturic/react-service/raw/97a7227ab3e2535b7a6924eb027db8a0f33dbb0f/packages/react-as-a-service/externals/raas.png "Raas")

## Packages

This repository is a monorepo that we manage using [Lerna](https://github.com/lerna/lerna). That means that we actually publish [several packages](/packages) to npm from the same codebase, including:

| Package                                                                      | Description                                                                        |
| ---------------------------------------------------------------------------- | ---------------------------------------------------------------------------------- |
| [`react-as-a-service`](/packages/react-as-a-service)                         | The core of Raas                                                                   |
| [`react-as-a-service-examples`](/packages/react-as-a-service-examples)       | The examples on how to use Raas                                                    |
