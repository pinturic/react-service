import React, { Component } from 'react';
import {
  ReactService,
  ExpressConnector,
  Route,
  Renderers,
  FileStorage
} from '@raas/raas';

import HelloWorldComponent from './HelloWorld';

export default app => {
  const reactService = new ReactService({
    connector: new ExpressConnector({
      application: app
    }),
    routes: [new Route({
      path: '/test/hello-world',
      async: false,
      priority: 0,
      renderer: Renderers.REACT_DOM,
      component: HelloWorldComponent
    }),]
  });
  return reactService;
};
