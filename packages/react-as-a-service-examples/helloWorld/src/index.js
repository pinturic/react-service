import express from "express";
import setupRaas from "./setupRaas";

const app = express();
setupRaas(app);

app.listen(8080, "localhost", () => {
  console.log(`Server listening on port 8080`);
});

