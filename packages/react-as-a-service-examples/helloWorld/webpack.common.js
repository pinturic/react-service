const webpack = require("webpack");
const path = require("path");

const CleanWebpackPlugin = require("clean-webpack-plugin");

module.exports = {
	entry: ["./src/index"],
	target: "node",
	node: {
		__filename: true,
		__dirname: true
	},
	module: {
		rules: [{
			test: /\.jsx?$/,
			use: [{
				loader: "babel-loader",
				options: {
					babelrc: false,
					presets: [
						["env", {
							modules: false
						}], "stage-0", 'stage-1', 'stage-2', 'stage-3', 'react'
					],
					plugins: [
						"transform-regenerator",
						"transform-runtime"
					]
				}
			}],
			exclude: /node_modules/
		}]
	},
	resolve: {
		modules: [path.resolve(__dirname, 'node_modules')],
	    extensions: ['.js', '.jsx'],
	},
	plugins: [
		new CleanWebpackPlugin(["dist"]),
		new webpack.NamedModulesPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
	],
	output: {
		path: path.join(__dirname, "dist"),
		filename: "server.js"
	}
};
