# Raas

There are few examples that show many examples of usage of Raas:

| **Test**        | Description                                                                            |
|-----------------|----------------------------------------------------------------------------------------|
| **HelloWorld**  | A minimal setup to print a HelloWorld                                                  |
| **Basic**       | Shows a setup with two components that return operations on the data passed om tje url |
| **Cluster**     | Shows how to setup a cluster of services with Raas                                     |
| **HighCharts**  | An example with HihCharts                                                              |
| **Plugins**     | An example with two custom plugins for Raas                                            |
| **ReduxRouter** | An example showing how to use redux and react-router with Raas                         |