import React, { Component } from 'react';

let result = null;

/**
 * A simple component that returns the sum of two values.
 * Just go to this url:
 * http://localhost:8080/test/add/2/3
 */
class AddComponent extends Component {
  constructor(props) {
    super(props);
  }

  static fetchData = ({ params }) =>
    new Promise(resolve =>
      setTimeout(() => {
        result = Number.parseInt(params.first) + Number.parseInt(params.second);
        resolve(result);
      }, 1000)
    );

  render() {
    return <div>{result}</div>;
  }
}

export default AddComponent;
