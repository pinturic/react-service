import React, { Component } from 'react';

let result = null;

/**
 * A simple component that prints Hello World.
 * Just go to this url:
 * http://localhost:8080/test/hello-world
 */
class HelloWorldComponent extends Component {
  constructor(props) {
    super(props);
  }

  static fetchData = ({ params }) => new Promise(resolve =>
    setTimeout(() => {
      result = 'Hello World!!!';
      resolve(result);
    }, 1000)
  );

  render() {
    return <div>{result}</div>;
  }
}

export default HelloWorldComponent;
