import {
  ReactServicePlugin
} from '@raas/raas';
import zlib from 'zlib';

/**
 * A plugin that counts the number of words in the rendered html
 */
export class WordCountPlugin extends ReactServicePlugin {
  constructor() {
    super({ name: 'WordCount' });
  }

  render = async ({
    route,
    location,
    params,
    query,
    html,
    obj
  }) => {
    console.log(`Rendering ${JSON.stringify(route)}, ${location}, ${JSON.stringify(params)}, ${JSON.stringify(query)}, ${html}`);
    return {
      wordCount: html.split(" ").length
    };
  }
};


/**
 * A plugin that returns a base64 representation of the zipped html
 */
export class CompressPlugin extends ReactServicePlugin {
  constructor() {
    super({ name: 'Compress' });
  }

  render = async ({
    route,
    location,
    params,
    query,
   html
  }) => {
    console.log(`Rendering ${JSON.stringify(route)}, ${location}, ${JSON.stringify(params)}, ${JSON.stringify(query)}, ${html}`);
    const res = zlib.gzipSync(html).toString('base64');
	return {
	  gzip: res
	}
  }
};

