import cluster from 'cluster';
import express from 'express';
import setupRaas from './setupRaas';

if (cluster.isMaster) {
  // Create 4 workers
  for (var i = 0; i < 4; i++) {
    cluster.fork();
  }
  // Listen for dying workers
  cluster.on('exit', function (worker) {
  console.log('Worker %d died :(', worker.id);
    cluster.fork();
  });
} else {
  const app = express();
  setupRaas(app);

  app.listen(8080, "localhost", () => {
    console.log(`Server listening on port 8080`);
  });
}

