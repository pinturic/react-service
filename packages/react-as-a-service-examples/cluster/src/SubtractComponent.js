import React, { Component } from 'react';

let result = null;

/**
 * A simple component that returns the difference of two values.
 * Just go to this url:
 * http://localhost:8080/test/subtract?first=2&second=3
 */
class SubtractComponent extends Component {
  constructor(props) {
    super(props);
  }

  static fetchData = ({ query }) => new Promise(resolve =>
    setTimeout(() => {
      result = Number.parseInt(query.first) - Number.parseInt(query.second);
      resolve(result);
    }, 1000)
  );

  render() {
    return <div>{result}</div>;
  }
}

export default SubtractComponent;
