import React, { Component } from 'react';
import ReactHighcharts from 'react-highcharts';

let points = [];

/**
 * A simple component that returns a line series painted with Highcharts:
 * you can test it by going to this url after the server is running:
 * http://localhost:8080/test/highcharts/3,8,5,1,2,3,4,6,7
 * 
 * Be careful because few settings for Highcharts are really important.
 * All the animations are to be disabled and forExport is to be set by using:
 * - animation: false,
 * - forExport: true,
 */
class HigchartsComponent extends Component {
  constructor(props) {
    super(props);
  }

  static fetchData = ({ params }) => new Promise(resolve => {
    points = params.linePoints
      .split(',')
      .map(p => Number.parseInt(p));
    resolve(points);
  });

  render() {
    const config = {
      chart: {
        // animation: false is mandatory for the highcharts to render on server
        animation: false,
        // forExport: true is mandatory to build the optimal html for export
        forExport: true
      },
      // tooltip is mandatory when forExport is true
      tooltip: {
        
      },

      title: {
        text: 'Highcharts example'
      },

      subtitle: {
          text: 'Done with Raas'
      },

      yAxis: {
          title: {
              text: 'Y value'
          }
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
      },

      plotOptions: {
        series: {
          // animation: false is mandatory for the highcharts to render on server
          animation: false,
          label: {
            connectorAllowed: false
          },
        }
      },

      series: [{
        name: "Series",
        data: points
      }]
    };
    return <div><ReactHighcharts config={config} /></div>;
  }
}

export default HigchartsComponent;
