import { applyMiddleware, createStore, combineReducers } from 'redux';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import rootReducer from './rootReducer';

export default (history, initialState) => {
  const browserMiddleware = routerMiddleware(history);
  const createStoreWithMiddleware = applyMiddleware(
    browserMiddleware
  )(createStore);
  const store = createStoreWithMiddleware(combineReducers({
    add: rootReducer,
    router: routerReducer
  }), initialState);

  return store;
};


