const initialState = {
  value: null
};

export default function current(state = initialState, action) {
  switch (action.type) {
    case 'ADD_VALUE':
      return {
        value: action.first + action.second
      };
    default:
      return state;
  }
};
