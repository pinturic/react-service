import React, { Component } from 'react';
import {
  ReactService,
  ExpressConnector,
  Route,
  Renderers,
  FileStorage
} from '@raas/raas';

import AddComponent from './AddComponent';

export default app => {
  const reactService = new ReactService({
    connector: new ExpressConnector({
      application: app
    }),
    storage: new FileStorage({
      folder: './tmp',
      ttl: 24 * 60 * 60, // = 1 day
      cleanInterval: 60 * 60 // = 1 hour
    }),
    routes: [new Route({
      path: '/test/add/:first/:second',
      async: false,
      priority: 0,
      renderer: Renderers.REACT_DOM,
      component: AddComponent
    }),]
  });
  return reactService;
};
