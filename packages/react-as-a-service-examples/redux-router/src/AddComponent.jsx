import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import createMemoryHistory from 'history/createMemoryHistory';
import { bindActionCreators } from 'redux';
import url from 'url';
import configureStore from './configureStore';

const setValue = (first, second) => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(store.dispatch({
        type: 'ADD_VALUE',
        first,
        second,
      }));
    }, 1000)
  })
};

const mapStateToProps = state => ({
  result: state.add.value
});

/**
 * A simple component that returns the sum of two values.
 * Just go to this url:
 * http://localhost:8080/test/add/2/3
 */
class AddComponent extends Component {
  constructor(props) {
    super(props);
  }

  /**
   * This is the function that Raas calls to compute/collected the needed data.
   * It needs a promise that is to be resolved when the results are ready.
   *
   * @param  {Object} options.params The params available
   * @return {Promise}                A promise resolved when the results are
   *                                 ready
   */
  static fetchData = ({ params }) =>
    setValue(
      Number.parseInt(params.first), 
      Number.parseInt(params.second)
    );

  render() {
    return <div>{this.props.result}</div>;
  }  
}

AddComponent = connect(mapStateToProps)(AddComponent);

/**
 * The initial state for the store
 */
let initialState;
/**
 * The history used by redux roter
 */
let history;
/**
 * The redux store
 */
let store;

class ReduxAddComponent extends Component {
  constructor(props) {
    super(props); 
  }

  static fetchData = ({ params, location }) => {
    const urlObject = url.parse(location);
    initialState = { };
    history = createMemoryHistory();
    store = configureStore(history, initialState);
    
    // the following two operations are done to 
    // "manually setup" the router store state
    store.getState().router.location = {
      pathname: urlObject.pathname,
      search: urlObject.search
    };
    history.push({
      pathname: urlObject.pathname,
      search: urlObject.search
    });

    return AddComponent.fetchData({ params });
  }

  render() {
    console.log(`Location is: ${JSON.stringify(store.getState().router.location)}`);
    return (
      <Provider store={store}>
        <ConnectedRouter
          history={history}
        >
          <AddComponent />
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default ReduxAddComponent;
