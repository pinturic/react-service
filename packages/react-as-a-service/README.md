![ Version ](https://img.shields.io/npm/v/@raas/raas.svg)
![ Downloads ](https://img.shields.io/npm/dm/@raas/raas.svg)
![ Issues ](https://img.shields.io/bitbucket/issues-raw/pinturic/react-service.svg)
![ Code documentation ](https://bitbucket.org/pinturic/react-service/raw/f7d65bc4e11a0135c7a54e615194ed574544540d/packages/react-as-a-service/esdoc/badge.svg)

![raas](https://bitbucket.org/pinturic/react-service/raw/97a7227ab3e2535b7a6924eb027db8a0f33dbb0f/packages/react-as-a-service/externals/raas.png "Raas")

# Raas

[React](https://facebook.github.io/react) as a service.

* [What](#what)
* [Installation](#installation)
* [Easy setup](#easy-setup)
* [Esdoc](#esdoc)
* [Examples](#examples)
* [Contributing](#contributing)
* [Issues](#issues)

## What

Generating html server side is a wonderful idea!
You can reuse your client-side React components on the server.
You can even emulate on the server a browser environment in order to use
the huge amount of client-side libraries that can help you in generating a rich
HTML experience.

## Why

* periodic/scheduled reports
* embed HTML in your email campaigns
* reuse yor client side components on a server 

## Installation

Using [npm](https://www.npmjs.com/):

    $ npm install --save @raas/raas

## Easy setup

The following example shows how to startup with a minimal setup with:
* raas
* express
* a HelloWorld react component

```js
import React, { Component } from 'react';
import {
  ReactService,
  ExpressConnector,
  Route,
  Renderers,
  FileStorage
} from '@raas/raas';

import HelloWorldComponent from './HelloWorld';

export default app => {
  const reactService = new ReactService({
    connector: new ExpressConnector({
      application: app
    }),
    routes: [new Route({
      path: '/test/hello-world',
      async: false,
      priority: 0,
      renderer: Renderers.REACT_DOM,
      component: HelloWorldComponent
    }),]
  });
  return reactService;
};
```

## Esdoc

Raas comes with a detailed documentation for developers that can be found in the local folder *esdoc* 

## Examples

The example libraries is full of full-fledged examples with Express that will help you in
setting up a working server in order to generate the your HTML.
Examples are [available here](https://bitbucket.org/pinturic/react-service/src/master/packages/react-as-a-service-examples/) 

## Contributing

Contributions are welcome so please create a pull request when you want to:

* improve the code or the documentation
* add a new cool feature
* fix bugs
* add new tests

## Issues

If you find a bug, please file an issue on [our issue tracker on BitBucket](https://bitbucket.org/pinturic/react-service/issues).