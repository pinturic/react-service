import getNewDocument from './renderers/jsdom/serverDom';
// so that document and window are available right now
// highcharts (and possibly other imported libraries will need it)
getNewDocument();

export { default as ReactService } from './services/ReactService';
export { default as ExpressConnector } from './connectors/ExpressConnector';
export { default as Route } from './router/Route';
export * as Renderers from './renderers/constants';
export { default as ReactServiceConnector } from
  './connectors/ReactServiceConnector';
export { default as ReactServiceStorage } from
  './storage/ReactServiceStorage';
export { default as FileStorage } from './storage/FileStorage';
export { default as ReactServicePlugin } from './plugins/ReactServicePlugin';
