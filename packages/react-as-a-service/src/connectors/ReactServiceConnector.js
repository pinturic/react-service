import url from 'url';

/**
 * The base class for the ReactServiceConnectors
 *
 * @example
 * connect = ({ routes, renderQueue, storage }) => {
 *    ...
 * }
 *
 * @interface
 */
class ReactServiceConnector {
  /**
   * The constructor for ReactServiceConnector
   * @param  {Object} options.application The application on which to connect
   * @return {ReactServiceConnector}      The ReactServiceConnector
   */
  constructor({ application }) {
    /**
     * The application on which to connect
     * @type {Object}
     */
    this.application = application;
  }

  /**
   * Connects the routes to the express application
   *
   * @param {Array<Route>} options.routes
   *        The array of routes to mount
   * @param {RenderQueue} options.renderQueue
   *        The queue of the rendering tasks
   * @param {ReactServiceStorage} options.storage
   *        The storage used to save and retrieve the files
   * @return {Boolean}                True if the operation successes
   */
  // eslint-disable-next-line no-unused-vars
  connect = ({ routes, renderQueue, storage }) => {
    throw new Error('Not implemented!!!');
  }

  /**
   * Gets an html from the storage if it is ready otherwise returns
   *
   * @param  {Object} req   The express request object
   * @param  {Object} res   The express response object
   * @return {Void} void
   */
  getByToken = async (req, res) => {
    const location = req.url;
    const { query } = url.parse(location, true);
    const { token } = query;
    const result = await this.storage.get(token);
    if (!result) {
      res.status(404).send('Document not ready');
    } else {
      res.send({
        html: result
      });
    }
  }
}

export default ReactServiceConnector;
