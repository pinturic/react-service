/* eslint-disable no-await-in-loop */
import express from 'express';
import { matchRoutes } from 'react-router-config';
import { v4 as uuidv4 } from 'uuid';
import url from 'url';
import ReactServiceConnector from './ReactServiceConnector';
import logger from '../utils/logger';

/**
 * The connector to use for Express
 */
class ExpressConnector extends ReactServiceConnector {
  /**
   * The constructor for the ExpressConnector
   * @param  {Object} options.application   The server application
   * @return {ExpressConnector}             The ExpressConnector
   */
  constructor({ application }) {
    super({ application });
    /**
     * The array of routes to mount
     * @type {Array<Route>}
     */
    this.routes = null;
    /**
     * The queue of the rendering tasks
     * @type {RenderQueue}
     */
    this.renderQueue = null;
    /**
     * The storage used to save and retrieve the files
     * @type {ReactServiceStorage}
     */
    this.storage = null;
  }

  /**
   * Connects the routes to the express application
   *
   * @param {Array<Route>} options.routes
   *        The array of routes to mount
   * @param {RenderQueue} options.renderQueue
   *        The queue of the rendering tasks
   * @param {ReactServiceStorage} options.storage
   *        The storage used to save and retrieve the files
   * @return {Boolean}                True if the operation successes
   */
  connect = ({ routes, renderQueue, storage }) => {
    this.renderQueue = renderQueue;
    this.routes = routes;
    this.storage = storage;
    const router = express.Router();
    routes.forEach((route) => {
      router.get(route.path, this.match);
    });
    router.get('/getByToken', this.getByToken);
    this.application.use('/', router);
    return true;
  }

  /**
   * Enriches the final object with the results coming from the plugins
   *
   * @param  {Route} options.route      The Route corresponding the rendered url
   * @param  {string} options.location  The location of the called endpoint
   * @param  {Object} options.params    The params passed in the url
   * @param  {Object} options.query     The object representing the parsed query
   *                                    string
   * @param  {string} options.html      The rendered html
   * @param  {Object} options.obj       The object to be returned to the client
   * @return {Void} void
   */
  computePlugins = async ({
    route,
    location,
    params,
    query,
    html,
    obj
  }) => {
    for (let i = 0; i < route.plugins.length; i++) {
      const plugin = route.plugins[i];
      let pluginResult = null;
      try {
        pluginResult = await plugin.render({
          route,
          location,
          params,
          query,
          html
        });
      } catch (e) {
        logger.error(`Error in rendering ${plugin.name}: location ${location}`);
      }
      Object.entries(pluginResult)
        .forEach((pair) => {
          const [key, value] = pair;
          obj[key] = value;
        });
    }
  }

  /**
   * Matches a request agains the routes and in case triggers the rendering
   *
   * @param  {Object} req   The express request object
   * @param  {Object} res   The express response object
   * @return {Void} Void
   */
  match = async (req, res) => {
    const location = req.url;
    const branch = matchRoutes(this.routes, req.path);
    const { query } = url.parse(location, true);
    if (branch.length > 0) {
      const { route, match } = branch[0];
      const result = this.renderQueue.enqueue({
        route,
        location,
        match,
        params: match.params,
        query
      });
      if (route.async) {
        const token = uuidv4();
        res.send({
          token
        });
        result.then(async (html) => {
          const saveValue = {
            id: token,
            html
          };
          await this.computePlugins({
            route,
            location,
            params: match.params,
            query,
            html,
            obj: saveValue,
          });
          this.storage.save(saveValue);
        });
      } else {
        const html = await result;
        const retValue = {
          html
        };
        await this.computePlugins({
          route,
          location,
          params: match.params,
          query,
          html,
          obj: retValue,
        });
        res.send(retValue);
      }
    } else {
      const error = `Route not found ${location}`;
      logger.error(error);
      res.status(404).send(`Not found route ${location}`);
    }
  }
}

export default ExpressConnector;
