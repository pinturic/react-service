import REACT_DOM from '../renderers/constants';

/**
 * A structure difining the route to serve
 */
class Route {
  /**
   * The constructor for route
   *
   * @param  {String}  options.path
   *         The path to serve
   * @param  {Boolean} [options.async=true]
   *         Wether to wait for the rendering to complete before returning the
   *         rendering result.
   *         If true when the endpoint is called a token will be returned; this
   *         token can be used to poll to get the rendering result.
   *         If false calling the endpoint will return directly the rendering
   *         result.
   *         The default is true.
   * @param  {string}  [options.renderer=REACT_DOM]
   *         It can be {@link REACT_DOM} or {@link JSDOM}.
   *         REACT_DOM will use the fast React dom renderToString
   *         https://reactjs.org/docs/react-dom-server.html#rendertostring
   *         JSDOM will use a full-fledged document created with jsdom; this is
   *         useful to emulate on the server side a browser environment and
   *         tipically to use js client side library that forcibly need a
   *         document object (like highcharts https://www.highcharts.com/).
   *         The default is REACT_DOM
   * @param  {React.Component}  options.component
   *         The component that will be used for endering
   * @param  {Number} [options.priority=0]
   *         Priority of operation. Operations with greater priority will be
   *         scheduled first.
   * @params {Array<ReactServicePlugin>}
   *         An array of plugins that will be used to post-process the rendered
   *         html.
   * @return {Route}                    The Route object
   *
   */
  constructor({
    path, async = true, renderer = REACT_DOM, component, priority = 0,
    plugins = []
  }) {
    /**
     * The path to serve
     * @type {String}
     */
    this.path = path;
    /**
     * Wether to wait for the rendering to complete before returning the
     *         rendering result.
     *         If true when the endpoint is called a token will be returned;
     *         this token can be used to poll to get the rendering result.
     *         To get the document then you can simply poll the server using
     *         the following endpoint
     *
     *         @example
     *         http://<server_address/getBytoken?token=<token>
     *
     *         If false calling the endpoint will return directly the rendering
     *         result.
     *         The default is true.
     * @type {Boolean}
     */
    this.async = async;
    /**
     *  {string}  [options.renderer=REACT_DOM]
     *         It can be {@link REACT_DOM} or {@link JSDOM}.
     *         REACT_DOM will use the fast React dom renderToString
     *         https://reactjs.org/docs/react-dom-server.html#rendertostring
     *         JSDOM will use a full-fledged document created with jsdom; this
     *         is useful to emulate on the server side a browser environment and
     *         tipically to use js client side library that forcibly need a
     *         document object (like highcharts https://www.highcharts.com/).
     *         The default is REACT_DOM
     * @type {string}
     */
    this.renderer = renderer;
    /**
     * The component that will be used for endering
     * @type {React.Component}
     */
    this.component = component;
    /**
     * Priority of operation. Operations with greater priority will be
     *         scheduled first.
     * @type {Number}
     */
    this.priority = priority;
    /**
     * An array of plugins that will be used to post-process the rendered
     * html.
     * @type {Array<ReactServicePlugin>}
     */
    this.plugins = plugins;
  }
}

export default Route;
