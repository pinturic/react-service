import lockfile from 'lockfile';
import path from 'path';
import findRemoveSync from 'find-remove';
import fs from 'fs-extra';
import ReactServiceStorage from './ReactServiceStorage';
import logger from '../utils/logger';

/**
 * The default storage for the temporrary files
 */
class FileStorage extends ReactServiceStorage {
  /**
   * The constructor for the ReactServiceStorage
   *
   * @param  {Number} [options.ttl=86400]
   *         The maximum time (expressed in seconds) after which the file will
   *         be deleted. The default value is 86400 seconds that is equal to
   *         one day
   * @param  {Number} [options.cleanInterval=3600]
   *         The scheduling interval for the jobs that cleans the old files
   * @param  {string} [options.folder='./tmp']
   *         The folder in which tmp files are stored
   */
  constructor({ ttl = 86400, cleanInterval = 3600, folder = './tmp' }) {
    super({ cleanInterval });
    /**
     * The maximum time (expressed in seconds) after which the file will
     * be deleted. The default value is 86400 seconds that is equal to
     * one day
     * @type {Number}
     */
    this.ttl = ttl;
    /**
     * The folder in which tmp files are stored
     * @type {string}
     */
    this.folder = folder;
  }

  /**
   * Retrieves an html identified by the passed id
   *
   * @param  {String} id
   *         The id of the document to retrieve
   * @return {Promise<string>}
   *         A Promise resolved with the retrieved html
   */
  get = async (id) => {
    let data = null;
    try {
      data = await fs.readFile(path.resolve(this.folder, `${id}.json`), 'utf8');
    } catch (e) {
      // in this case the file is still not available
    }
    return data;
  }

  /**
   * Saves an html identified by the passed id
   * @param  {string} options.id
   *         The id of the document to retrieve
   * @param  {string} options.html
   *         The html to save
   * @return {Promise}
   *         A Promise resolved when the file is saved
   */
  save = async ({ id, html }) => {
    const res = await fs.outputFile(
      path.resolve(this.folder, `${id}.json`),
      html
    );
    return res;
  }

  /**
   * Clean all the file older than the specified ttl
   * @return {Object} Statistics about clean operation
   */
  clean = async () => new Promise((resolve, reject) => {
    const fileName = path.resolve(this.folder, 'clean.lock');
    lockfile.lock(
      fileName, {
        stale: 1000 * 60 * 60, // expire after one hour
      },
      (err) => {
        if (err) {
          logger.error('Error in retrieving lock: ', err);
          reject(err);
          return;
        }
        try {
          const deletedFiles = findRemoveSync(this.folder, {
            age: {
              seconds: this.ttl
            },
            extensions: '.json'
          });
          resolve({
            nDeleted: Object.keys(deletedFiles).size
          });
        } catch (e) {
          logger.error('Error in cleaning old documents: ', e);
          reject(e);
        }
        lockfile.unlock(fileName, (er) => {
          if (er) {
            logger.error('Error in releasing lock: ', er);
            reject(er);
          }
        });
      }
    );
  });
}

export default FileStorage;
