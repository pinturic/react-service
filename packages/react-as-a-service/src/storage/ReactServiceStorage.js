import logger from '../utils/logger';

/**
 * The base class for the ReactServiceStorage
*/
class ReactServiceStorage {
  /**
   * The constructor for the ReactServiceStorage
   *
   * @param  {Number} [options.cleanInterval=3600]
   *         The scheduling interval for the jobs that cleans the old files
   */
  constructor({ cleanInterval = 3600 }) {
    /**
     * The scheduling interval for the jobs that cleans the old files
     * @type {Number}
     */
    this.cleanInterval = cleanInterval;
    setInterval(async () => {
      const cleanResults = await this.clean();
      logger.info(`Clean stats: ${JSON.stringify(cleanResults)}`);
    }, cleanInterval * 1000);
  }

  /**
   * Saves an html identified by the passed id
   * @param  {string} options.id
   *         The id of the document to retrieve
   * @param  {string} options.html
   *         The html to save
   * @return {Promise}
   *         A Promise resolved when the file is saved
   */
  save = () => {
    throw new Error('Not implemented!!!');
  }

  /**
  * Clean all the file older than the specified ttl
  * @return {Object} Statistics about clean operation
  */
  clean = () => {
    throw new Error('Not implemented!!!');
  }

  /**
   * Retrieves an html identified by the passed id
   *
   * @param  {String} id
   *         The id of the document to retrieve
   * @return {Promise<string>}
   *         A Promise resolved with the retrieved html
  */
  get = () => {
    throw new Error('Not implemented!!!');
  }
}

export default ReactServiceStorage;
