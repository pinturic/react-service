/**
 * The base class for the ReactServicePlugin.
 * The deriving classes must implement an aync method called render and return
 * an object with the key/value pairs that are to be returned together with the
 * rendered html:
 *
 * @example
 * render = async ({ }) => {
 *   ...
 *   return {
 *     myKey: myValue
 *   }
 * }
 *
 * @interface
*/
class ReactServicePlugin {
  /**
   * The name of the plugin
   *
   * @param  {string} options.name  The name of the plugin
   * @return {Void} void
   */
  constructor({ name }) {
    this.name = name;
  }

  render = () => {
    throw new Error('Not implemented!!!');
  }
}

export default ReactServicePlugin;
