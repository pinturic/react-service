/* eslint-disable max-len */
import PQueue from 'p-queue';

/**
 * The queue of jobs to be rendered
 */
class RenderQueue {
  /**
   * The constructor for the render queue
   * @param  {Renderer} options.renderer The rendering service
   */
  constructor({ renderer }) {
    /**
     * The rendering service
     * @type {Renderer}
    */
    this.renderer = renderer;
    /**
     * The queue of the pending erndering tasks
     * @type {PQueue}
     */
    this.queue = new PQueue({ concurrency: 1 });
  }

  /**
   * Enqueue a new job for rendering
   * @param  {Route} options.route
   *         The matched route
   * @param  {String} options.location
   *         The location of the called endpoint
   * @param  {Object} options.query
   *         An object containing the parameters of the queryString
   * @param  {Object} options.params
   *         An object containing the path parameters matched for the rendered
   *         route
   * @param  {Object} options.match
   *         A match object as defined here: @see
   *         https://github.com/ReactTraining/react-router/blob/master/packages/react-router/docs/api/match.md
   * @return {Promise<string>}
   *         A promise that resolves with the rendered html
   */
  enqueue = async ({
    route, location, query, params, match
  }) => this.queue.add(() => this.renderer.render({
    route,
    location,
    query,
    params,
    match
  }), {
    priority: route.priority
  });
}

export default RenderQueue;
