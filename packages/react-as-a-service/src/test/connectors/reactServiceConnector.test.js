import chai from 'chai';
import sinon from 'sinon';
import express from 'express';

import ReactServiceConnector from '../../connectors/ReactServiceConnector';

const should = chai.should();
const sandbox = sinon.createSandbox();
const expect = chai.expect;

const app = express();
let connector = null;

describe('ReactServiceConnector test', () => {
  before(() => {
    connector = new ReactServiceConnector({
      application: app
    });
    connector.storage = {
      get: () => new Promise(resolve => resolve('<div/>'))
    };
  });

  it('ReactServiceConnector constructor', (done) => {
    expect(connector.application).to.equal(app);
    done();
  });

  it('Connect should throw an exception', (done) => {
  	expect(() => connector.connect({})).to.throw(Error, /Not implemented/);
    done();
  });

  it('getByToken should return html', (done) => {
  	const req = {
  	  url: 'http://testUrl.com?token=token'
  	};
  	let result = null;
  	const res = {
  	  send: ({ html }) => result = html
  	};
  	connector.getByToken(req, res)
  	  .then(() => {
  	    expect(result).to.equal('<div/>');
    	  done();
  	  });
  });
});
