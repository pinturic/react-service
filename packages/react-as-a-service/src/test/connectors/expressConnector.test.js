import chai from 'chai';
import sinon from 'sinon';
import express from 'express';

import ExpressConnector from '../../connectors/ExpressConnector';

const should = chai.should();
const sandbox = sinon.createSandbox();
const expect = chai.expect;

const app = express();
let connector = null;

describe('ExpressConnector test', () => {
  before(() => {
    connector = new ExpressConnector({
      application: app
    });
  });

  it('ExpressConnector constructor', (done) => {
    expect(connector.application).to.equal(app);
    done();
  });
});
