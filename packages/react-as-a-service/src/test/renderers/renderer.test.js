import chai from 'chai';
import sinon from 'sinon';
import express from 'express';

import Renderer from '../../renderers/Renderer';
import Route from '../../router/Route';
import * as Renderers from '../../renderers/constants';

const should = chai.should();
const sandbox = sinon.createSandbox();
const expect = chai.expect;

let renderer = null;
let routes = null;

describe('Renderer test', () => {
  before(() => {
    routes = [new Route({
      path: '/test/hello-world',
      async: false,
      priority: 0,
      renderer: Renderers.REACT_DOM,
      component: null
    }), ];
    renderer = new Renderer({
      routes
    });
  });

  it('Renderer constructor', (done) => {
    expect(renderer.routes).to.equal(routes);
    expect(renderer.reactRenderer).to.be.a('object');
    expect(renderer.jsdomRenderer).to.be.a('object');
    done();
  });
});
