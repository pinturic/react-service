import chai from 'chai';
import sinon from 'sinon';
import express from 'express';

import JsdomRenderer from '../../renderers/JsdomRenderer';
import Route from '../../router/Route';
import * as Renderers from '../../renderers/constants';
import getNewDocument from '../../renderers/jsdom/serverDom';

const should = chai.should();
const sandbox = sinon.createSandbox();
const expect = chai.expect;

let jsdomRenderer = null;
let route = null;

describe('JsdomRenderer test', () => {
  before(() => {
    route = new Route({
      path: '/test/hello-world',
      async: false,
      priority: 0,
      renderer: Renderers.REACT_DOM,
      component: null
    });
    jsdomRenderer = new JsdomRenderer();
  });

  it('JsdomRenderer constructor', (done) => {
    done();
  });

  it('render should return empty string', (done) => {
    const html = jsdomRenderer.render({
      route
    });
    expect(html).to.equal(getNewDocument().documentElement.outerHTML);
    done();
  });
});
