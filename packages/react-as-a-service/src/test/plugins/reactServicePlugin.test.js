import chai from 'chai';
import sinon from 'sinon';
import express from 'express';

import ReactServicePlugin from '../../plugins/ReactServicePlugin';

const should = chai.should();
const sandbox = sinon.createSandbox();
const expect = chai.expect;

let reactServicePlugin = null;

describe('ReactServicePlugin test', () => {
  before(() => {
    reactServicePlugin = new ReactServicePlugin({
      name: 'Test Plugin'
    });
  });

  it('ReactServiceStorage constructor', (done) => {
    expect(reactServicePlugin.name).to.equal('Test Plugin');
    done();
  });

  it('render should throw an exception', (done) => {
    expect(reactServicePlugin.render).to.throw(Error, /Not implemented/);
    done();
  });
});
