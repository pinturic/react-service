import chai from 'chai';
import sinon from 'sinon';
import express from 'express';

import ReactService from '../../services/ReactService';
import ExpressConnector from '../../connectors/ExpressConnector';
import Route from '../../router/Route';
import * as Renderers from '../../renderers/constants';

const should = chai.should();
const sandbox = sinon.createSandbox();
const expect = chai.expect;

const app = express();
let reactService = null;
let connector = null;
let routes = null;

describe('ReactService test', () => {
  before(() => {
    connector = new ExpressConnector({
      application: app
    });
    routes = [new Route({
      path: '/test/hello-world',
      async: false,
      priority: 0,
      renderer: Renderers.REACT_DOM,
      component: null
    }), ];
    reactService = new ReactService({
      connector,
      routes
    });
  });

  it('ReactService constructor', (done) => {
    expect(reactService.connector).to.equal(connector);
    expect(reactService.routes).to.equal(routes);
    done();
  });
});
