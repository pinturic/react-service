import chai from 'chai';
import sinon from 'sinon';
import express from 'express';

import RenderQueue from '../../queue/RenderQueue';
import ReactRenderer from '../../renderers/ReactRenderer';
import Route from '../../router/Route';
import * as Renderers from '../../renderers/constants';

const should = chai.should();
const sandbox = sinon.createSandbox();
const expect = chai.expect;

let renderQueue = null;
let renderer = null;
let route = null;

describe('Queue test', () => {
  before(() => {
  	route = new Route({
      path: '/test/hello-world',
      async: false,
      priority: 0,
      renderer: Renderers.REACT_DOM,
      component: null
    });
    renderer = new ReactRenderer({
      route
    });
    renderQueue = new RenderQueue({
      renderer
    });
  });

  it('RenderQueue constructor', (done) => {
    expect(renderQueue.renderer).to.equal(renderer);
    done();
  });

  it('RenderQueue enqueue', (done) => {
    renderQueue
      .enqueue({
      	route
      })
      .catch(err => console.log(err))
      .then(() => done());
  });
});
