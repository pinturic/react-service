import chai from 'chai';
import sinon from 'sinon';
import express from 'express';

import ReactServiceStorage from '../../storage/ReactServiceStorage';

const should = chai.should();
const sandbox = sinon.createSandbox();
const expect = chai.expect;

let serviceStorage = null;

describe('ReactServiceStorage test', () => {
  before(() => {
    serviceStorage = new ReactServiceStorage({
      cleanInterval: 3600
    });
  });

  it('ReactServiceStorage constructor', (done) => {
    expect(serviceStorage.cleanInterval).to.equal(3600);
    done();
  });

  it('save should throw an exception', (done) => {
    expect(serviceStorage.save).to.throw(Error, /Not implemented/);
    done();
  });

  it('clean should throw an exception', (done) => {
    expect(serviceStorage.clean).to.throw(Error, /Not implemented/);
    done();
  });

  it('save should throw an exception', (done) => {
    expect(serviceStorage.get).to.throw(Error, /Not implemented/);
    done();
  });
});
