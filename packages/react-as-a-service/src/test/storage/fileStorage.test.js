import chai from 'chai';
import sinon from 'sinon';
import express from 'express';

import FileStorage from '../../storage/FileStorage';

const should = chai.should();
const sandbox = sinon.createSandbox();
const { assert, expect } = chai;

let serviceStorage = null;

describe('FileStorage test', () => {
  before(() => {
    serviceStorage = new FileStorage({
      cleanInterval: 3600,
      ttl: 86400,
      folder: './tmp',
    });
  });

  it('ReactServiceStorage constructor', (done) => {
    expect(serviceStorage.cleanInterval).to.equal(3600);
    expect(serviceStorage.ttl).to.equal(86400);
    expect(serviceStorage.folder).to.equal('./tmp');
    done();
  });

  it('get should return null', (done) => {
    serviceStorage
      .get(12345)
      .then((res) => {
        expect(res).to.equal(null);
        done();
      });
  });

  it('save file and get it', (done) => {
    serviceStorage
      .save({
        id: 54321,
        html: '<div/>'
      })
      .then(() => serviceStorage.get(54321))
      .then((res) => {
        expect(res).to.equal('<div/>');
        done();
      });
  });

  it('clean should clean the fs', (done) => {
    serviceStorage
      .clean()
      .then((res) => {
        should.exist(res);
        assert.isObject(res);
        done();
      });
  });
});
