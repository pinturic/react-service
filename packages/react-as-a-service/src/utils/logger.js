import log4js from 'log4js';

const logger = log4js.getLogger('raas');
logger.level = 'info';

export default logger;
