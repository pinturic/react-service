
/**
 * This renderer will use an emulated document based on Jsdom to render the
 * component
 * @type {String}
 */
export const JSDOM = 'JSDOM';

/**
 * This renderer will use React dom renderToString to render the component
 * https://reactjs.org/docs/react-dom-server.html#rendertostring
 * @type {String}
 */
export const REACT_DOM = 'REACT_DOM';
