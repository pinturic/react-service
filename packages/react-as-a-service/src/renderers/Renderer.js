/* eslint-disable max-len */
import logger from '../utils/logger';
import { REACT_DOM, JSDOM } from './constants';
import ReactRenderer from './ReactRenderer';
import JsdomRenderer from './JsdomRenderer';

/**
 * The Renderer class
 */
class Renderer {
  /**
   * The constructor for the render queue
   * @param  {Array<Route>} options.routes The array of available routes
   * @param  {Renderer} options.renderer The rendering service
   */
  constructor({ routes }) {
    /**
     * The array of available routes
     * @type {Array<Route>}
     */
    this.routes = routes;
    /**
     * The react renderer
     * @type {ReactRenderer}
     */
    this.reactRenderer = new ReactRenderer();
    /**
     * The JSDOM renderer
     * @type {ReactRenderer}
     */
    this.jsdomRenderer = new JsdomRenderer();
  }

  /**
   * Renders the route with the passed location
   *
   * @param  {Route} options.route      The matched Route
   * @param  {string} options.location  The called location
   * @param  {Object} options.query
   *         An object containing the parameters of the queryString
   * @param  {Object} options.params
   *         An object containing the path parameters matched for the rendered
   *         route
   * @param  {Object} options.match
   *         A match object as defined here: @see
   *         https://github.com/ReactTraining/react-router/blob/master/packages/react-router/docs/api/match.md
   * @return {Promise<string>}
   *         A Promise that resolves with the rendered html into a string
   */
  render = async ({
    route, location, query, params, match
  }) => {
    if (logger.isDebugEnabled()) {
      logger.debug(`Rendering: ${route.path} ${location} ${JSON.stringify(query)} ${JSON.stringify(params)} ${JSON.stringify(match)}`);
    }
    const rendererParams = {
      route,
      location,
      query,
      params,
      match
    };
    const dataResult = await route.component.fetchData(rendererParams);
    logger.info(`DataResult: ${JSON.stringify(dataResult)}`);
    if (route.renderer === REACT_DOM) {
      return this.reactRenderer.render(rendererParams);
    } if (route.renderer === JSDOM) {
      return this.jsdomRenderer.render(rendererParams);
    }
    return dataResult;
  }
}

export default Renderer;
