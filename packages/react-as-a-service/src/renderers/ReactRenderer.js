/* eslint-disable react/jsx-filename-extension */

import React from 'react';
import ReactDOMServer from 'react-dom/server';

/**
 * The React Rendered class
 */
class ReactRenderer {
  /**
   * Renders the route with the passed location
   * @param  {Route} options.route The route to render
   * @return {string}              The rendered HTML
   */
  render = ({ route }) => {
    const Component = route.component;
    if (Component) {
      return ReactDOMServer.renderToString(<Component />);
    }
    return '';
  }
}

export default ReactRenderer;
