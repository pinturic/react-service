/* eslint-disable no-cond-assign, no-param-reassign */

/* this polyfill is used for old browsers that do not have
  requestAnimationFrame */
import 'raf/polyfill';

const jsdom = require('jsdom');

/**
 * Returns a new Jsdoc document
 *
 * @return {Object} A new jsdoc document
 */
const getNewDocument = () => {
  const htmlString = '<!doctype html>'
    + '<html>'
      + '<head>'
      + '</head>'
      + '<body>'
        + '<div id="raas-container"></div>'
      + '</body>'
    + '</html>';

  // this is the logic used to shim the browser typical properties in the
  // node environment
  const dom = new jsdom.JSDOM(htmlString);
  const { document } = dom.window;
  global.document = dom.window.document;
  const window = document.defaultView;
  global.self = document.defaultView;
  global.window = document.defaultView;
  global.navigator = window.navigator;
  Object.getOwnPropertyNames(global).forEach((key) => {
    if (!window[key]) {
      window[key] = global[key];
    }
  });

  /**
   * Helper function to find the ancestor with the specified nodeName for the
   * specified dom element
   *
   * @param  {NodeElement} el       The node element
   * @param  {string} nodeName      The node name to search for
   * @return {NodeElement}         The found node element
   */
  function findAncestor(el, nodeName) {
    while ((el = el.parentElement) && el.nodeName !== nodeName);
    return el;
  }

  // From https://gist.github.com/TorsteinHonsi/e8a1e6971608523eb8dd
  // Do some modifications to the jsdom document in order to get the SVG
  // bounding boxes right.
  document.createElementNS = function (ns, tagName) {
    const elem = document.createElement(tagName);

    // Set private namespace to satisfy jsdom's getter
    elem._namespaceURI = ns; // eslint-disable-line no-underscore-dangle

    Object.defineProperty(elem, 'ownerSVGElement', {
      get() { return findAncestor(elem, 'SVG'); },
      enumerable: true,
      configurable: true
    });

    /**
     * Pass Highcharts' test for SVG capabilities
     * @returns {undefined}
     */
    elem.createSVGRect = function () {};
    /**
     * jsdom doesn't compute layout (see
     * https://github.com/tmpvar/jsdom/issues/135).
     * This getBBox implementation provides just enough information to get
     * Highcharts to render text boxes correctly, and is not intended to work
     * like a general getBBox implementation. The height of the boxes are
     * computed from the sum of tspans and their font sizes. The width is
     * based on an average width for each glyph. It could easily be improved to
     * take font-weight into account. For a more exact result we could to create
     * a map over glyph widths for several fonts and sizes, but it may not be
     * necessary for the purpose.
     * @returns {Object} The bounding box
     */
    elem.getBBox = function () {
      let lineWidth = 0;
      let width = 0;
      let height = 0;

      // all the magic numbers below are empirical values extracted from server
      // highcharts: https://gist.github.com/TorsteinHonsi/e8a1e6971608523eb8dd

      const elems = elem.children.length ? elem.children : [elem];

      [].forEach.call(elems, (child) => {
        let fontSize = child.style.fontSize || elem.style.fontSize;

        // The font size and lineHeight is based on empirical values, copied
        // from the SVGRenderer.fontMetrics function in Highcharts.
        if (/px/.test(fontSize)) {
          fontSize = parseInt(fontSize, 10);
        } else {
          fontSize = /em/.test(fontSize) ? parseFloat(fontSize) * 12 : 12;
        }
        const lineHeight = fontSize < 24
          ? fontSize + 3
          : Math.round(fontSize * 1.2);
        const textLength = child.textContent.length * fontSize * 0.55;

        // Tspans on the same line
        if (child.getAttribute('dx') !== '0') {
          height += lineHeight;
        }

        // New line
        if (child.getAttribute('dy') !== null) {
          lineWidth = 0;
        }

        lineWidth += textLength;
        width = Math.max(width, lineWidth);
      });

      return {
        x: 0,
        y: 0,
        width,
        height
      };
    };
    return elem;
  };

  return document;
};

export default getNewDocument;
