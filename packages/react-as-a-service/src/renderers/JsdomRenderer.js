/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import ReactDOM from 'react-dom';
import getNewDocument from './jsdom/serverDom';
import logger from '../utils/logger';

/**
 * The React Rendered class
 */
class JsdomRenderer {
  /**
   * Renders the route with the passed location
   * @param  {Route} options.route The route to render
   * @return {string}              The rendered HTML
   */
  render = ({ route }) => {
    const Component = route.component;
    const document = getNewDocument();
    const container = document.getElementById('raas-container');

    try {
      if (Component) {
        ReactDOM.render(
          <Component />,
          container
        );
      }
    } catch (errorRendering) {
      logger.error(`Error in rendering ${JSON.stringify(route)}`);
    }
    const componentHTML = document.documentElement.outerHTML;
    return componentHTML;
  }
}

export default JsdomRenderer;
