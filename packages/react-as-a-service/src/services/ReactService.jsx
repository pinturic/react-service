import RenderQueue from '../queue/RenderQueue';
import Renderer from '../renderers/Renderer';
import FileStorage from '../storage/FileStorage';

/**
 * The React Service main class
 */
class ReactService {
  /**
   * The constructor for ReactService
   *
   * @param  {ReactServiceConnector} options.connector
   *         The connector used by the server
   * @param  {Array<Route>} options.routes
   *         The array of routes the application will serve
   * @param  {ReactServiceStorage} [options.storage=FileStorage]
   *         The backend used to store the temporary files
   * @return {ReactService}
   *         The ReactService
   */
  constructor({ connector, routes, storage }) {
    /**
     * The connector used by the server
     * @type {ReactServiceConnector}
     */
    this.connector = connector;
    /**
     * The array of routes the application will serve
     * @type {Array<Route>}
     */
    this.routes = routes;
    /**
     * The backend used to store the temporary files
     * @type {ReactServiceStorage}
     */
    this.storage = storage;
    if (!this.storage) {
      this.storage = new FileStorage({
        folder: './tmp'
      });
    }
    /**
     * The renderer service
     * @type {Renderer}
     */
    this.renderer = new Renderer({ routes });
    /**
     * The queue of the rendering jobs
     * @type {RenderQueue}
     */
    this.renderQueue = new RenderQueue({
      renderer: this.renderer
    });
    this.connector.connect({
      routes,
      renderQueue: this.renderQueue,
      storage: this.storage
    });
  }
}

export default ReactService;
