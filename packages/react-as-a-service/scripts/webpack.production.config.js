const chalk = require('chalk');
const path = require('path');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const webpack = require('webpack');
const CleanWebpackPlugin = require("clean-webpack-plugin");

const srcFolder = path.join(__dirname, '../src');

module.exports = {
  entry: ['babel-polyfill','./src'],
  target: 'node',
  externals: [
    nodeExternals(),
  ],
  output: {
    path: path.resolve(__dirname, '../lib'),
    filename: 'raas.js',
    libraryTarget: 'umd',
  },
  resolve: {
    alias: {
    },
    extensions: ['.js', '.jsx'],
    modules: [
      path.resolve(__dirname, './src'),
      'node_modules'
    ],
  },
  cache: false,
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        query: {
          cacheDirectory: true,
          plugins: [],
          presets: [
            'stage-1',
            'stage-2',
            'stage-3',
            [
              'env',
              {
                targets: {
                  node: '8.0'
                },
                debug: true,
                useBuiltIns: false
              }
            ]
          ]
        },
        include: [srcFolder]
      },
      {
        test: /\.jsx?$/,
        loader: 'eslint-loader',
        include: [srcFolder]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(["../lib"], {verbose: true, allowExternal: true}),
    new ProgressBarPlugin({
      format: `build [:bar] ${chalk.green.bold(':percent')} (:elapsed seconds)`,
      clear: false,
      width: 60
    })
  ],
  stats: {
    chunks: false,
    modules: false,
  },
};